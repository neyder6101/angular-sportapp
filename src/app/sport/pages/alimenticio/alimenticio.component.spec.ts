import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlimenticioComponent } from './alimenticio.component';

describe('AlimenticioComponent', () => {
  let component: AlimenticioComponent;
  let fixture: ComponentFixture<AlimenticioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlimenticioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AlimenticioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
