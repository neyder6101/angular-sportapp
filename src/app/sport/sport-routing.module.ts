import { NgModule } from '@angular/core';
import { AlimenticioComponent } from './pages/alimenticio/alimenticio.component';
import { DemograficoComponent } from './pages/demografico/demografico.component';
import { DeportivoComponent } from './pages/deportivo/deportivo.component';
import { HomeComponent } from './pages/home/home.component';

import { RouterModule, Routes } from '@angular/router';

const rutas: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: 'demografico', component: DemograficoComponent },
      { path: 'deportivo', component: DeportivoComponent },
      { path: 'alimenticio', component: AlimenticioComponent },      
      { path: '**', redirectTo: 'demografico' }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild( rutas )
  ],
  exports: [
    RouterModule
  ]
})
export class SportRoutingModule { }
