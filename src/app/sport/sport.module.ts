import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemograficoComponent } from './pages/demografico/demografico.component';
import { DeportivoComponent } from './pages/deportivo/deportivo.component';
import { AlimenticioComponent } from './pages/alimenticio/alimenticio.component';
import { HomeComponent } from './pages/home/home.component';
import { SportRoutingModule } from './sport-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '../material/material.module';


@NgModule({
  declarations: [
    DemograficoComponent,
    DeportivoComponent,
    AlimenticioComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    SportRoutingModule
  ]
})
export class SportModule { }
